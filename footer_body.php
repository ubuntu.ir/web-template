	</div>
</div>


<footer class="pt-4 my-md-5 pt-md-5 border-top container">
	<div class="row">
		<div class="col-sm-2">
			<h5><a class="link-secondary text-decoration-none" href="https://ubuntu-ir.org">
				اوبونتو
			</a></h5>
			<ul class="list-unstyled text-small">
				<li class="mb-1">
					<a class="link-secondary text-decoration-none" href="https://forum.ubuntu-ir.org/index.php?board=38">
						اخبار
					</a>
				</li>
				<li class="mb-1">
					<a class="link-secondary text-decoration-none" href="https://www.ubuntu.com/download">
						دریافت
					</a>
				</li>
			</ul>
		</div>
		<div class="col-sm-2">
			<h5><a class="link-secondary text-decoration-none" href="https://forum.ubuntu-ir.org">
				انجمن‌ها
			</a></h5>
			<ul class="list-unstyled text-small">
				<li class="mb-1">
					<a class="link-secondary text-decoration-none" href="https://forum.ubuntu-ir.org/index.php?topic=242">
						قوانین
					</a>
				</li>
				<li class="mb-1">
					<a class="link-secondary text-decoration-none" href="https://forum.ubuntu-ir.org/index.php?action=register">
						ثبت‌نام
					</a>
				</li>
			</ul>
		</div>
		<div class="col-sm-2">
			<h5><a class="link-secondary text-decoration-none" rel="me" href="https://persadon.com/@UbuntuIranParty">
				اجتماعی
			</a></h5>
			<ul class="list-unstyled text-small">
				<li class="mb-1">
					<a class="link-secondary text-decoration-none" href="https://wiki.ubuntu-ir.org">
						ویکی
					</a>
				</li>
				<li class="mb-1">
					<a class="link-secondary text-decoration-none" href="https://ubuntu-ir.org/irc">
						کانال IRC
					</a>
				</li>
			</ul>
		</div>
		<div class="col-md" id="copyright">
			<small class="mb-1 link-secondary">
				&copy;
				 ۱۴۰۲ جامعهٔ فارسی‌زبان اوبونتو
				<br />
				&copy;
				 ۲۰۲۳ شرکت کنونیکال
				<br />
				 اوبونتو و کنونیکال نام‌های تجاری متعلق به Canonical Ltd هستند.
			</small>
			<img class="mb-1" id="ubuntuir_logo_footer" src="https://static.ubuntu-ir.org/images/logo_footer.png" alt="اوبونتو ایران">
		</div>
	</div><!-- row -->
</footer>
