<!--	<div style="position: fixed; color: #fff; background: #000; box-shadow: 0 0 0 999px #000; clip-path: inset(0 -100%); inset: 0 auto auto 0; transform-origin: 100% 0; transform: translate(-29.3%) rotate(-45deg); z-index: 5; font-size: 16px;"><a href="https://fa.wikipedia.org/wiki/%D9%85%D9%87%D8%B3%D8%A7_%D8%A7%D9%85%DB%8C%D9%86%DB%8C" style="color: #fff; text-decoration: none;" target="_blank">مهسا امینی</a></div> -->
	<div id="ubuntuir_header">
		<a id="ubuntuir_logo" href="https://ubuntu-ir.org/">
			<img src="https://static.ubuntu-ir.org/images/logo.png" alt="لوگو">
		</a>
		<div id="ubuntuir_header_navbar">
		<ul>
			<li <?php if ($tpl["page"] == "main") { ?>class="ubuntuir_header_active"<?php } ?>>
				<a href="https://ubuntu-ir.org">اوبونتو</a>
			</li>
			<li>
				<a href="https://forum.ubuntu-ir.org">انجمن‌های پشتیبانی</a>
			</li>
			<li <?php if ($tpl["page"] == "wiki") { ?>class="ubuntuir_header_active"<?php } ?>>
				<a href="https://wiki.ubuntu-ir.org">ویکی فارسی</a>
			</li>
			<li <?php if ($tpl["page"] == "videos") { ?>class="ubuntuir_header_active"<?php } ?>>
				<a href="https://videos.ubuntu-ir.org">ویدیوها</a>
			</li>
			<li <?php if ($tpl["page"] == "events") { ?> class="ubuntuir_header_active"<?php } ?>>
				<a href="https://events.ubuntu-ir.org">همایش‌ها</a>
			</li>
			<li <?php if ($tpl["page"] == "irc") { ?>class="ubuntuir_header_active"<?php } ?>>
				<a href="https://ubuntu-ir.org/irc">کانال IRC</a>
			</li>
			<li <?php if ($tpl["page"] == "pastebin") { ?>class="ubuntuir_header_active"<?php } ?>>
				<a href="https://paste.ubuntu-ir.org">سرویس Pastebin</a>
			</li>
		</ul>
		</div>
	</div>
	<div id="ubuntuir_body">
		<div id="ubuntuir_body_text">
