<!doctype html>
<html dir="rtl" lang="fa">
<head>
	<title>Iranian Ubuntu Community</title>
	<meta charset="utf-8">
	<meta name="description" content="Iranian Ubuntu Community">
	<meta name="keywords" content="ubuntu, gnu, linux, iran, persian, parsi, farsi, اوبونتو, گنو, لینوکس, فارسی, پارسی, ایران">
	<meta name="author" content="Moein Alinaghian">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="https://static.ubuntu-ir.org/images/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="https://static.ubuntu-ir.org/css/ubuntuir.css?">
	<link href="https://static.ubuntu-ir.org/libs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<?php if (isset($tpl["css"])) { ?><link rel="stylesheet" type="text/css" href="<?=$tpl["css"]?>"><?php }; ?>
</head>
<body>
