	<script>
		// Activate the dark mode, if the user prefers it
		if (window.matchMedia("(prefers-color-scheme: dark)").matches)
			document.body.classList.add("dark-mode");
	</script>
	<!-- <script src="https://static.ubuntu-ir.org/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script> -->
</body>
</html>
